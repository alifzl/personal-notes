# Notes

> This repository will be outdated over time. I appreciate it if anyone helps me keep it up-to-date.

I believe technology helps us to think differently in every way. So I believe everyone deserves to learn things. Sometimes the hardest part of starting something is the first step in that direction, so these notes are going to help you with that very first step, and I hope you do the rest.

I am not going to teach you details of that technology, but I categorize everything so that you can find what you need quickly.

What I'm working on:
- [x] [Git](Tech%20Guide/Git/README.md)
- [x] [Vim](Tech%20Guide/Vim/README.md)
- [x] [Regex](Tech%20Guide/Regex/README.md)
- [x] [Tmux](Tech%20Guide/Tmux/README.md)
- [x] [Docker](Tech%20Guide/Docker/README.md)
- [ ] Docker Swarm
- [ ] Kubernetes
- [ ] [Shell Scripting](Tech%20Guide/Shell-Scripting/README.md)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change or add.
- Don't change this file.
- Please break your PR into different commits.
- Write good commit messages.
- If you forked this repository and want to change or add something, make sure to update it before your new PR, using `git pull`.

## License
This project is licensed under the GPL-3.0 License - see the [LICENSE](./LICENSE) file for details.
